public class Fish {                                             // like a "bike", but "fish"

    private int speed, swimingDepth, reaverDepth, reaverSpeed;

    Fish(){
        speed = 0;
        swimingDepth = 0;
        reaverDepth = 0;
        reaverSpeed = 0;

    }

    Fish (int a, int b, int c, int d){

        swimingDepth = b;          // b <= 0
        reaverDepth = c;            // c>=0
        reaverSpeed = d;        //if d < 0, it goes to the left, else to the right or water don't move at all
        speed = a + reaverSpeed;
    }

    public void deapthChanging(int a){
        if(swimingDepth + a > 0)
            swimingDepth = 0;
        else if (swimingDepth + a <= reaverDepth)
            swimingDepth = reaverDepth;
        else
            swimingDepth += a;
    }

    public void speedChanging(int a){
        speed += a;
    }

    public void getFishInf(){
        System.out.format("Depth of swimming is: " + swimingDepth +"\nSpeed of swimming is: " + speed + "\n\n");
    }
}

