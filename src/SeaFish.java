public class SeaFish extends Fish {
    int saltRateInWater;

    SeaFish (int a, int b, int c, int d, int e){
        super (a , b, c, d);
        saltRateInWater  = e;
    }

    @Override
    public void getFishInf() {
        super.getFishInf();
        System.out.format("How much salt in water: " + saltRateInWater + "\n");
    }
}
